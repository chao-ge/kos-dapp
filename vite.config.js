import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
const { resolve } = require('path');
// const APP_NAME = import.meta.env.VITE_APP_NAME
const APP_NAME = process.env.VITE_APP_NAME || 'Knights of Thrones';
const DEBUG = process.env.VITE_DEBUG || 'true';
const APP_TYPE = process.env.VITE_APP_TYPE || 'dapp';

// https://vitejs.dev/config/
export default defineConfig({
  // module.exports = {
  define: {
    __DEBUG__: DEBUG,
    __DAPP__: APP_TYPE === 'dapp',
    __APP_NAME__: "'" + APP_NAME + "'",
    __APP_TYPE__: "'" + APP_TYPE + "'", // app | dapp
  },
  plugins: [vue()],
  server: {
    port: 3003,
    proxy: {
      '/api': {
        // target: 'http://127.0.0.1:3002',
        target: 'https://kos.knightsofthrones.com',
        // target: 'https://xxx.io',
        ws: false,
        secure: false,
        changeOrigin: true,
        // pathRewrite:{
        //   '^/rng':''
        // }
      },
    },
  },
  resolve: {
    alias: {
      '@': resolve(__dirname,'src'),
      '@components': resolve(__dirname,'src', 'components'),
      '@views': resolve(__dirname,'src', 'views'),
    },
  },
  // cssPreprocessOptions: {
  //   scss: {
  //     additionalData: '@import "./src/sass/style.scss";'
  //   }
  // },
  css:{
    preprocessorOptions: {
      scss: {
        additionalData: `@import "./src/sass/style.scss";`
      }
    }
  }
});
