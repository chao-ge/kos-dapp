import { createI18n } from 'vue-i18n'
import messages from './index'

const localeData = {
    legacy: false, // composition API
    locale: 'zh-HK',
    globalInjection:true,
    messages,
};

export function setupI18n(app) {
    const i18n = createI18n(localeData);
    app.use(i18n);
}
