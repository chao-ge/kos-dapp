import en from "./en_us";
import hk from "./zh_hk";
import jp from "./ja_jp";
import kr from "./ko_kr";

const messages = {
    'en-US': {
        ...en
    },
    'zh-HK': {
        ...hk
    },
    'ja-JP': {
        ...jp
    },
    'ko-KR': {
        ...kr
    },

};

export default messages;
