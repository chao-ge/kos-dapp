import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router';

const routerHistory = createWebHistory();
import Page from './views/Page.vue';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Page,
        children: [
            {
                path: '/',
                name: 'Home',
                component: () => import('./views/Home/index.vue'),
                // component: App,
            },
            {
                path: '/InMoney',
                name: 'InMoney',
                component: () => import('./views/InMoney/index.vue'),
            },
            {
                path: '/TurnMoney',
                name: 'TurnMoney',
                component: () => import('./views/TurnMoney/index.vue'),
            },
            {
                path: '/MyUnion',
                name: 'MyUnion',
                component: () => import('./views/MyUnion/index.vue'),
            },
            {
                path: '/LpMiningDetails',
                name: 'LpMiningDetails',
                component: () => import('./views/LpMiningDetails/index.vue'),
            },
            {
                path: '/LpMining',
                name: 'LpMining',
                component: () => import('./views/LpMining/index.vue'),
            },
            {
                path: '/TransferOut',
                name: 'TransferOut',
                component: () => import('./views/TransferOut/index.vue'),
            },
            {
                path: '/MoneyPackage',
                name: 'MoneyPackage',
                component: () => import('./views/MoneyPackage/index.vue'),
            },
            {
                path: '/MyPackageCard',
                name: 'MyPackageCard',
                component: () => import('./views/MyPackageCard/index.vue'),
            },
            {
                path: '/BlindBoxShop',
                name: 'BlindBoxShop',
                component: () => import('./views/BlindBoxShop/index.vue'),
            },
            {
                path: '/BuyBlindBox',
                name: 'BuyBlindBox',
                component: () => import('./views/BuyBlindBox/index.vue'),
            },
            {
                path: '/OpenBlindBox',
                name: 'OpenBlindBox',
                component: () => import('./views/OpenBlindBox/index.vue'),
            },
            {
                path: '/TruckMining',
                name: 'TruckMining',
                component: () => import('./views/TruckMining/index.vue'),
            },
            {
                path: '/SelectCard',
                name: 'SelectCard',
                component: () => import('./views/SelectCard/index.vue'),
            },
            {
                path: '/RankingList',
                name: 'RankingList',
                component: () => import('./views/RankingList/index.vue'),
            },
            {
                path: '/ExtractHistory',
                name: 'ExtractHistory',
                component: () => import('./views/ExtractHistory/index.vue'),
            },
            {
                path: '/DepositLog',
                name: 'DepositLog',
                component: () => import('./views/DepositLog/index.vue'),
            },
            {
                path:'/UpgradeUnion',
                name:'UpgradeUnion',
                component: () => import('./views/UpgradeUnion/index.vue'),
            },
            {
                path:'/ApplyUnion',
                name:'ApplyUnion',
                component: () => import('./views/ApplyUnion/index.vue'),
            },
            {
                path:'/AuditMember',
                name:'AuditMember',
                component: () => import('./views/AuditMember/index.vue'),
            }
        ],
    },
    {
        path: '/InvitationCode',
        name: 'InvitationCode',
        component: () => import('./views/InvitationCode/index.vue'),
    },
];

const router = createRouter({
    history: routerHistory,
    routes: routes,
});

export default router;
