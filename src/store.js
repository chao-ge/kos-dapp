import {createStore} from 'vuex'

const store = createStore({
    state: {
        isOverlay: false,
        OverlayName:'',
        OverlayParams:{},
        UnionName:{},
        popupType:false,
        selectType: false
    },
    mutations: {
        changeOverlay(state, isTrue) {
            state.isOverlay = isTrue;
            if(!isTrue){
                state.OverlayName = 'div';
            }
        },
        changeUnionName(state, item) {
            state.UnionName = item
        },
        changeOverlayName(state, {name, params}) {
            state.OverlayName = name;
            state.isOverlay = true;
            state.OverlayParams = params;
        },
        changePopupType (state, type) {
            state.popupType = type
        },
        changeSelectType (state, type) {
            state.selectType = type
        }
    },
    actions: {},
    getters: {}
});

export default store
