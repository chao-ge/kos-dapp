const APP_NAME = __APP_NAME__;

document.title = APP_NAME;

import { createApp } from 'vue';

import App from './App.vue';

import vant, { Toast } from 'vant';
import 'amfe-flexible/index.js';
import router from './router';
import store from './store.js';
import 'vant/lib/index.css';

import { setupI18n } from './lang/setup';

import './css/common.css';
const app = createApp(App);

setupI18n(app);

window.Toast = Toast;

app.use(router);
app.use(store);
app.use(vant);

app.mount('#app');
